


Meteor.publish('sales', function () {
  return Sales.find();
});

Meteor.publish('inventory', function () {
  return Inventory.find();
});

Meteor.publish('test', function () {
  return Test.find();
});

Meteor.publish('purchases', function () {
  return Purchases.find();
});

Meteor.publish('customers', function () {
  return Customers.find();
});