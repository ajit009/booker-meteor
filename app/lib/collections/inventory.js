Inventory = new Mongo.Collection('inventory');


if (Meteor.isServer) {
  Inventory.allow({
    insert: function (userId, doc) {
      
      return Meteor.user();
    },

    update: function (userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function (userId, doc) {
      return true;
    }
  });

}
