Purchases = new Mongo.Collection('purchases');


if (Meteor.isServer) {
  Purchases.allow({
    insert: function (userId, doc) {
      return true;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function (userId, doc) {
      return true;
    }
  });

  
}
