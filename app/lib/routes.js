Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});


Router.route('/', {
  name: 'home',
  controller: 'HomeController',
  where: 'client'
});

Router.route('sales', {
  name: 'sales',
  controller: 'SalesController',
  where: 'client'
});

Router.route('inventory', {
  name: 'inventory',
  controller: 'InventoryController',
  where: 'client'
});

Router.route('dashboard', {
  name: 'dashboard',
  controller: 'DashboardController',
  where: 'client'
});

Router.route('login', {
  name: 'login',
  controller: 'LoginController',
  where: 'client'
});

Router.route('test', {
  name: 'test',
  controller: 'TestController',
  where: 'client'
});

Router.route('register', {
  name: 'register',
  controller: 'RegisterController',
  where: 'client'
});

Router.route('purchases', {
  name: 'purchases',
  controller: 'PurchasesController',
  where: 'client'
});

Router.route('purchases/:_id', {
  name: 'purchasesLocal',
  path: '/purchases/:_id',
  subscriptions: function() {
    this.subscribe('inventory');
    this.subscribe('purchases');
    this.subscribe('inventory', this.params._id).wait();
  },
  data: function () {
    return Inventory.findOne({_id: this.params._id});
  },
  controller: 'PurchasesController',
  where: 'client'
});





Router.route('customers', {
  name: 'customers',
  controller: 'CustomersController',
  where: 'client'
});