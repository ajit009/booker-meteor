/*****************************************************************************/
/* Dashboard: Event Handlers */
/*****************************************************************************/
Template.Dashboard.events({
	
});

/*****************************************************************************/
/* Dashboard: Helpers */
/*****************************************************************************/
Template.Dashboard.helpers({

	settings: function () {
        return {
            collection: Inventory,
            rowsPerPage: 5,
            showFilter: true,
            fields: [
            	{ key: 'itemName', label: 'Name' },
			    { key: 'itemCategory', label: 'Category' },
			    { key: 'units', label: 'In Stock' },
			    { key: 'itemSellingPrice', label: 'Selling Price' },
			    { key: 'notes', label: 'Instructions' },
			    { key: 'discounts', label: 'Discounts' } 
            ]
        };
    },
});

/*****************************************************************************/
/* Dashboard: Lifecycle Hooks */
/*****************************************************************************/
Template.Dashboard.onCreated(function () {
});

Template.Dashboard.onRendered(function () {
});

Template.Dashboard.onDestroyed(function () {
});
