/*****************************************************************************/
/* Purchases: Event Handlers */
/*****************************************************************************/
Template.Purchases.events({
	 'click .reactive-table tbody tr': function (event) {
    // 'click #mainTab' : function (event) {
    var purchase = this;
    var _id=	purchase.id;
    Router.go('/purchases/'+purchase._id);

  }
});

/*****************************************************************************/
/* Purchases: Helpers */
/*****************************************************************************/
Template.Purchases.helpers({
  
  purchaseCollection : function(){

		return Purchases.find().fetch();
	},

	settings: function () {
        return {
            collection: Inventory,
            rowsPerPage: 5,
            showFilter: true,
            fields: [
            	{ key: 'itemName', label: 'Name' },
			    { key: 'itemCategory', label: 'Category' },
			    { key: 'units', label: 'In Stock' }
            	]
        };
    },
    settingsForEvents: function () {
        return {
            collection: Purchases,
            rowsPerPage: 5,
            showFilter: true
            
        };
    }


});

/*****************************************************************************/
/* Purchases: Lifecycle Hooks */
/*****************************************************************************/
Template.Purchases.onCreated(function () {
});

Template.Purchases.onRendered(function () {
});

Template.Purchases.onDestroyed(function () {
});
