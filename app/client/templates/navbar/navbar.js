/*****************************************************************************/
/* Navbar: Event Handlers */
/*****************************************************************************/
Template.Navbar.events({
	'click #logoutBtn' : function(e,t){
		Meteor.logout();
		Router.go('/home');
	}
});

/*****************************************************************************/
/* Navbar: Helpers */
/*****************************************************************************/
Template.Navbar.helpers({
});

/*****************************************************************************/
/* Navbar: Lifecycle Hooks */
/*****************************************************************************/
Template.Navbar.onCreated(function () {
});

Template.Navbar.onRendered(function () {
});

Template.Navbar.onDestroyed(function () {
});
