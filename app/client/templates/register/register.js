/*****************************************************************************/
/* Register: Event Handlers */
/*****************************************************************************/
Template.Register.events({
	'submit #register-form' : function(e, t){
      e.preventDefault();
      var _email = t.find('#inputEmail').value;
      var _password = t.find('#inputPassword').value;
      var _name	=	t.find('#inputName').value;

        Accounts.createUser({ 
        	username: _name, 
        	email : _email,
        	password: _password

        }, function(err){
        if (err) {
        	alert("Registration failed");
        }else{
          Router.go('/dashboard');
        }
      });

        t.find('#inputEmail').value='';
      	t.find('#inputPassword').value='';
      	t.find('#inputName').value='';
         return false; 
         
      }
});

/*****************************************************************************/
/* Register: Helpers */
/*****************************************************************************/
Template.Register.helpers({
});

/*****************************************************************************/
/* Register: Lifecycle Hooks */
/*****************************************************************************/
Template.Register.onCreated(function () {
});

Template.Register.onRendered(function () {
});

Template.Register.onDestroyed(function () {
});
