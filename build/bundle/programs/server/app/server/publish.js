(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// server/publish.js                                                   //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
                                                                       //
                                                                       //
Meteor.publish('sales', function () {                                  // 4
  return Sales.find();                                                 // 5
});                                                                    //
                                                                       //
Meteor.publish('inventory', function () {                              // 8
  return Inventory.find();                                             // 9
});                                                                    //
                                                                       //
Meteor.publish('test', function () {                                   // 12
  return Test.find();                                                  // 13
});                                                                    //
                                                                       //
Meteor.publish('purchases', function () {                              // 16
  return Purchases.find();                                             // 17
});                                                                    //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=publish.js.map
