(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/routes.js                                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
Router.configure({                                                     // 1
  layoutTemplate: 'MasterLayout',                                      // 2
  loadingTemplate: 'Loading',                                          // 3
  notFoundTemplate: 'NotFound'                                         // 4
});                                                                    //
                                                                       //
Router.route('/', {                                                    // 8
  name: 'home',                                                        // 9
  controller: 'HomeController',                                        // 10
  where: 'client'                                                      // 11
});                                                                    //
                                                                       //
Router.route('sales', {                                                // 14
  name: 'sales',                                                       // 15
  controller: 'SalesController',                                       // 16
  where: 'client'                                                      // 17
});                                                                    //
                                                                       //
Router.route('inventory', {                                            // 20
  name: 'inventory',                                                   // 21
  controller: 'InventoryController',                                   // 22
  where: 'client'                                                      // 23
});                                                                    //
                                                                       //
Router.route('dashboard', {                                            // 26
  name: 'dashboard',                                                   // 27
  controller: 'DashboardController',                                   // 28
  where: 'client'                                                      // 29
});                                                                    //
                                                                       //
Router.route('login', {                                                // 32
  name: 'login',                                                       // 33
  controller: 'LoginController',                                       // 34
  where: 'client'                                                      // 35
});                                                                    //
                                                                       //
Router.route('test', {                                                 // 38
  name: 'test',                                                        // 39
  controller: 'TestController',                                        // 40
  where: 'client'                                                      // 41
});                                                                    //
                                                                       //
Router.route('register', {                                             // 44
  name: 'register',                                                    // 45
  controller: 'RegisterController',                                    // 46
  where: 'client'                                                      // 47
});                                                                    //
                                                                       //
Router.route('purchases', {                                            // 50
  name: 'purchases',                                                   // 51
  controller: 'PurchasesController',                                   // 52
  where: 'client'                                                      // 53
});                                                                    //
                                                                       //
Router.route('purchases/:_id', {                                       // 56
  name: 'purchasesLocal',                                              // 57
  path: '/purchases/:_id',                                             // 58
  subscriptions: function () {                                         // 59
    this.subscribe('inventory');                                       // 60
    this.subscribe('purchases');                                       // 61
    this.subscribe('inventory', this.params._id).wait();               // 62
  },                                                                   //
  data: function () {                                                  // 64
    return Inventory.findOne({ _id: this.params._id });                // 65
  },                                                                   //
  controller: 'PurchasesController',                                   // 67
  where: 'client'                                                      // 68
});                                                                    //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=routes.js.map
