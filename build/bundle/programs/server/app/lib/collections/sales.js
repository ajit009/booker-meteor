(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/collections/sales.js                                            //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
Sales = new Mongo.Collection('sales');                                 // 1
                                                                       //
if (Meteor.isServer) {                                                 // 4
  Sales.allow({                                                        // 5
    insert: function (userId, doc) {                                   // 6
      return false;                                                    // 7
    },                                                                 //
                                                                       //
    update: function (userId, doc, fieldNames, modifier) {             // 10
      return false;                                                    // 11
    },                                                                 //
                                                                       //
    remove: function (userId, doc) {                                   // 14
      return false;                                                    // 15
    }                                                                  //
  });                                                                  //
                                                                       //
  Sales.deny({                                                         // 19
    insert: function (userId, doc) {                                   // 20
      return true;                                                     // 21
    },                                                                 //
                                                                       //
    update: function (userId, doc, fieldNames, modifier) {             // 24
      return true;                                                     // 25
    },                                                                 //
                                                                       //
    remove: function (userId, doc) {                                   // 28
      return true;                                                     // 29
    }                                                                  //
  });                                                                  //
}                                                                      //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=sales.js.map
