(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/collections/purchases.js                                        //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
Purchases = new Mongo.Collection('purchases');                         // 1
                                                                       //
if (Meteor.isServer) {                                                 // 4
  Purchases.allow({                                                    // 5
    insert: function (userId, doc) {                                   // 6
      return true;                                                     // 7
    },                                                                 //
                                                                       //
    update: function (userId, doc, fieldNames, modifier) {             // 10
      return true;                                                     // 11
    },                                                                 //
                                                                       //
    remove: function (userId, doc) {                                   // 14
      return true;                                                     // 15
    }                                                                  //
  });                                                                  //
}                                                                      //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=purchases.js.map
