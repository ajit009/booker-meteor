(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/collections/test.js                                             //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
Test = new Mongo.Collection('test');                                   // 1
                                                                       //
if (Meteor.isServer) {                                                 // 4
  Test.allow({                                                         // 5
    insert: function (userId, doc) {                                   // 6
      return false;                                                    // 7
    },                                                                 //
                                                                       //
    update: function (userId, doc, fieldNames, modifier) {             // 10
      return false;                                                    // 11
    },                                                                 //
                                                                       //
    remove: function (userId, doc) {                                   // 14
      return false;                                                    // 15
    }                                                                  //
  });                                                                  //
                                                                       //
  Test.deny({                                                          // 19
    insert: function (userId, doc) {                                   // 20
      return true;                                                     // 21
    },                                                                 //
                                                                       //
    update: function (userId, doc, fieldNames, modifier) {             // 24
      return true;                                                     // 25
    },                                                                 //
                                                                       //
    remove: function (userId, doc) {                                   // 28
      return true;                                                     // 29
    }                                                                  //
  });                                                                  //
}                                                                      //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=test.js.map
