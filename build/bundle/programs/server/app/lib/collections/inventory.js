(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/collections/inventory.js                                        //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
Inventory = new Mongo.Collection('inventory');                         // 1
                                                                       //
if (Meteor.isServer) {                                                 // 4
  Inventory.allow({                                                    // 5
    insert: function (userId, doc) {                                   // 6
                                                                       //
      return Meteor.user();                                            // 8
    },                                                                 //
                                                                       //
    update: function (userId, doc, fieldNames, modifier) {             // 11
      return true;                                                     // 12
    },                                                                 //
                                                                       //
    remove: function (userId, doc) {                                   // 15
      return true;                                                     // 16
    }                                                                  //
  });                                                                  //
}                                                                      //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=inventory.js.map
